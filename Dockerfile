FROM tiangolo/meinheld-gunicorn-flask:python3.7

ENV APP_MODULE "app:create_app()"

COPY texts/ /app/texts
COPY static/ /app/static

COPY requirements.txt /app

RUN pip install -r /app/requirements.txt

COPY \
  app.py \
  markov.py \
  dictogram.py histogram.py sample.py \
  linked_queue.py linkedlist.py \
  /app/
