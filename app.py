import flask
import sys
import random
import markov

def create_app():
    app = flask.Flask(__name__)

    with open('./texts/metro.txt') as file:
        my_text = file.read()

    words = markov.get_words(my_text)
    app.markov = markov.MarkovChain(words, order=3)
    app.rng = random.Random()

    @app.route('/')
    def root():
        return app.send_static_file('app.html')

    @app.route("/api")
    def api():
        seed = int(flask.request.args.get('seed') or app.rng.randrange(sys.maxsize))

        random.seed(seed)

        return flask.jsonify({
            "seed": str(seed),
            "paragraph": app.markov.walk(5)
        })

    return app
